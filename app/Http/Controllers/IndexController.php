<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NewsController;

class IndexController extends Controller {

	public function index() {
		return view('index',[
			'news' => (new NewsController())->index()
		]);
	}

}
