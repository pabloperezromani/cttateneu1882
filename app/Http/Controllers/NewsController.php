<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Pic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\UserNotLoggedInException;

class NewsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('news.index', ['news' => News::orderBy('timestamp', 'desc')->paginateFirstDifferent(4,8)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\NewsRequest  $newsRequest
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $newsRequest) {
        $this->update($newsRequest, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return view('news.show', ['news' => News::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\NewsRequest  $newsRequest
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $newsRequest, $id) {
        $news = $id ? News::find($id) : new News();
        $news->summary = $newsRequest->get('summary');
        $news->text = $newsRequest->get('text');
        $news->timestamp = $newsRequest->get('timestamp');
        $news->image_id = $newsRequest->get('image_id') ?? Pic::where('url', $newsRequest->get('image_url'))->first() ? Pic::where('url', $newsRequest->get('image_url'))->first()->id : null;
        $news->save();
    }

}
