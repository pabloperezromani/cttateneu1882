<?php

namespace App\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Auth;

class NewsRequest extends CustomFormRequest {

	public function rules() {
		return [
			'summary' => 'nullable|string',
			'text' => 'nullable|string',
			'timestamp' => 'required|date_format:"Y-m-d H:i"',
			'pic_id' => 'nullable|exists:pics,id',
			'pic_url' => 'nullable|string'
		];
	}

	public function authorize() {
		return Auth::user() && Auth::user()->hasRole('editor');
	}

}
