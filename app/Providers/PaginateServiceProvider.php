<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class PaginateServiceProvider extends ServiceProvider
{
    public function register()
    {
        Collection::make(glob(__DIR__ . '/Paginate/CollectionMacros/*.php'))
            ->map(function ($path) {
                return pathinfo($path, PATHINFO_FILENAME);
            })
            ->reject(function ($macro) {
                return Collection::hasMacro($macro);
            })
            ->each(function ($macro) {
                $macroClass = 'App\\Providers\\Paginate\\CollectionMacros\\' . $macro;
                Collection::macro(Str::camel($macro), app($macroClass)());
            });

        Collection::make(glob(__DIR__ . '/Paginate/BuilderMacros/*.php'))
            ->map(function ($path) {
                return pathinfo($path, PATHINFO_FILENAME);
            })
            ->reject(function ($macro) {
                return Builder::hasGlobalMacro($macro);
            })
            ->each(function ($macro) {
                $macroClass = 'App\\Providers\\Paginate\\BuilderMacros\\' . $macro;
                Builder::macro(Str::camel($macro), app($macroClass)());
            });
    }
}
