<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Vite;
use Illuminate\Pagination\Paginator;
use App\Models\Language;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Vite::macro('image', fn (string $asset) => $this->asset("resources/images/{$asset}"));
		view()->composer('partials.language_switcher', function ($view) {
			$view->with('current_language', Language::where('iso_639-1', app()->getLocale())->first());
			$view->with('available_languages', Language::all());
		});
    }
}
