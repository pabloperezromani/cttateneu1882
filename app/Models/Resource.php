<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resource extends Authenticatable {

	use HasFactory, SoftDeletes;

	/**
	 * The "type" of the auto-incrementing ID.
	 * 
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = [
		'uri',
		'owner_user_id'
	];

}
