<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
	use HasFactory, SoftDeletes;

	/**
	 * The "type" of the auto-incrementing ID.
	 * 
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = [
		'iso_639-1'
	];
}
