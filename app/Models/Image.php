<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Resource;

class Image extends Resource {
	
	use HasFactory, SoftDeletes;

	/**
	 * @var array
	 */
	protected $fillable = [
	];

}
