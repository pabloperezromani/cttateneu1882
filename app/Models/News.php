<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $pic_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $summary
 * @property string $text
 * @property string $timestamp
 * @property boolean $active
 * @property Pic $pic
 * @property NewsTag[] $newsTags
 * @property SpecialNews[] $specialNews
 */
class News extends Model {
	
	use HasFactory, SoftDeletes;

	/**
	 * The "type" of the auto-incrementing ID.
	 * 
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = [
		'main_image_id',
		'timestamp',
	];

}
