<!DOCTYPE html>
<html class="wide wow-animation" lang="ca">
	<head>
		<title>CTT Ateneu 1882</title>
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<link rel="icon" href="{{ Vite::image('favicon.ico') }}" type="image/x-icon">
		<style>
			#loading-icon {
				background-position:center;
				background-size:50px;
				background-repeat:no-repeat;
				width:100%;
				height:100%;
				position:absolute;
				background-image:url({{ Vite::image('loading.png') }});
			}
			#start {
				height:0;
				visibility:hidden;
			}
		</style>
		@vite('resources/js/app.js')
	</head>
	<body>
		<div id='loading-icon'/></div>
	<div class="page" id="start">
		<header class="section page-header rd-navbar-darker">
			<div class="rd-navbar-wrap">
				<nav class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="114px" data-xl-stick-up-offset="114px" data-xxl-stick-up-offset="114px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
					<div class="rd-navbar-panel">
						<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-main"><span></span></button>
						<div class="rd-navbar-panel-inner container">
							<div class="rd-navbar-collapse rd-navbar-panel-item">
								<!--<div class="owl-carousel-navbar owl-carousel-inline-outer-1">
								  <div class="owl-inline-nav">
									<p class="heading-7">Hot News</p>
									<button class="owl-arrow owl-arrow-prev"></button>
									<button class="owl-arrow owl-arrow-next"></button>
								  </div>
								  <div class="owl-carousel-inline-wrap">
									<div class="owl-carousel owl-carousel-inline letter-spacing-075" data-items="1" data-autoplay="true" data-autoplay-speed="3200" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="10" data-mouse-drag="false" data-touch-drag="false" data-nav-custom=".owl-carousel-navbar">
									  <p>Game 2: 24 Second Thoughts before Start</p>
									  <p>Game 6: 35 Second Thoughts before Start</p>
									  <p>Game 11: 10 Second Thoughts before Start</p>
									</div>
								  </div>
								</div>-->
							</div>
							<div class="rd-navbar-panel-item rd-navbar-panel-list">
								<ul class="list-inline list-inline-middle list-inline-sm list-inline-bordered">
									<li>
										<ul class="list-inline list-inline-xs">
											<li class="rd-mail-link">
												<div class="unit unit-horizontal unit-spacing-xs">
													<div class="unit-body">
														<a class="link-light" href="mailto:info@cttateneu1882.com">
															<span class="icon icon-secondary icon-xs fa fl-bigmug-line-email64"></span>info@cttateneu1882.com</span>
														</a>
													</div>
												</div>
											</li>
											<li class="rd-mail-link">
												<div class="unit unit-horizontal unit-spacing-xs">
													<div class="unit-body">
														<a class="link-light" target="_blank" href="https://wa.me/633109111">
															<span class="icon icon-secondary icon-xs fa fa-whatsapp"></span>633 109 111
														</a>
													</div>
												</div>
											</li>
											<li class="rd-mail-link">
												<div class="unit unit-horizontal unit-spacing-xs">
													<div class="unit-body">
														<a class="link-light" href="tel:633109111">
															<span class="icon icon-xs fa fa-phone"></span>
														</a>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<ul class="list-inline list-inline-xs">
											<li><a class="icon icon-xs icon-light fa fa-facebook" target="_blank" href="https://www.facebook.com/cttateneu1882"></a></li>
											<li><a class="icon icon-xs icon-light fa fa-twitter" target="_blank" href="https://twitter.com/cttateneu1882"></a></li>
											<li><a class="icon icon-xs icon-light fa fa-instagram" target="_blank" href="https://www.instagram.com/cttateneu1882/"></a></li>
										</ul>
									</li>
									<li>
										@include('partials/language_switcher')
									</li>
								</ul>
							</div>
							<!--<div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>-->
						</div>
					</div>
					<div class="rd-navbar-main rd-navbar-dark">
						<div class="rd-navbar-main-container container">
							<div class="rd-navbar-brand"><a class="brand" href="#start"><img class="brand-logo brand-logo-dark" src="{{ Vite::image('logo-print-hd-transparent.png') }}" alt="" width="129" height="81"/><img class="brand-logo brand-logo-light" src="{{ Vite::image('logo-print-hd-transparent.png') }}" alt="" width="129" height="81"/></a></div>
							<ul class="rd-navbar-nav"> 
								<li class="rd-nav-item"><a class="rd-nav-link" href="#" anchor="#news">Notícies</a>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="#">Galeria</a>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="#">El club</a>
									<ul class="rd-menu rd-navbar-dropdown">
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="shop.html">Història</a></li>
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="product-page.html">Ubicació</a></li>
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="shopping-cart.html">Equips i jugadors</a></li>
									</ul>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="#">L'escola</a>
									<ul class="rd-menu rd-navbar-dropdown">
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="shop.html">Vine a conèixer-nos</a></li>
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="product-page.html">Horaris</a></li>
										<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="shopping-cart.html">Quotes</a></li>
									</ul>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="#">Contacte</a>
								</li>
							</ul>
							<div class="rd-navbar-main-element">
								<ul class="list-inline list-inline-bordered">
									<!--<li class="rd-navbar-search-wrap">
									  <div class="rd-navbar-search">
										<button class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search"><span></span></button>
										<form class="rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
										  <div class="form-wrap">
											<label class="form-label" for="rd-navbar-search-form-input">Enter your search request here...</label>
											<input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
											<div class="rd-search-results-live" id="rd-search-results-live"></div>
										  </div>
										  <button class="rd-search-form-submit fl-budicons-launch-search81" type="submit"></button>
										</form>
									  </div>
									</li>-->
									<li><a class="link link-icon link-icon-left link-classic" href="login-and-register.html"><span class="icon fl-bigmug-line-login12"></span></a></li>
									<li>
										<!--<div class="cart-inline-toggled-outer">
										  <a class="link-cart-hidden-link" href="shopping-cart.html"></a>
										  <button class="link link-cart cart-inline-toggle" data-rd-navbar-toggle="#cart-inline"><span class="link-cart-icon fl-bigmug-line-shopping202"></span><span class="link-cart-counter">2</span></button>
										  <article class="cart-inline cart-inline-toggled" id="cart-inline">
											<div class="cart-inline-inner">
											  <div class="cart-inline-header">
												<h5 class="cart-inline-title">In cart: 2 products</h5>
												<p class="cart-inline-subtitle">total price: $580</p>
											  </div>
											  <div class="cart-inline-main">
													  <article class="product-inline">
														<div class="product-inline-aside"><a class="product-inline-figure" href="product-page.html"><img class="product-inline-image" src="{{ Vite::image('shop/product-cart-1.png') }}" alt=""/></a></div>
														<div class="product-inline-main">
														  <p class="heading-7 product-inline-title"><a href="product-page.html">Nike Air Zoom Pegasus</a></p>
														  <ul class="product-inline-meta">
															<li>
															  <input class="form-input" type="number" data-zeros="true" value="1" min="1" max="100">
															</li>
															<li>
															  <p class="product-inline-price">$290.00</p>
															</li>
														  </ul>
														</div>
													  </article>
													  <article class="product-inline">
														<div class="product-inline-aside"><a class="product-inline-figure" href="product-page.html"><img class="product-inline-image" src="{{ Vite::image('shop/product-cart-2.png') }}" alt=""/></a></div>
														<div class="product-inline-main">
														  <p class="heading-7 product-inline-title"><a href="product-page.html">Nike Baseball Hat</a></p>
														  <ul class="product-inline-meta">
															<li>
															  <input class="form-input" type="number" data-zeros="true" value="1" min="1" max="100">
															</li>
															<li>
															  <p class="product-inline-price">$290.00</p>
															</li>
														  </ul>
														</div>
													  </article>
											  </div>
											  <div class="cart-inline-footer"><a class="button button-md button-default-outline" href="shopping-cart.html">Go to Cart</a><a class="button button-md button-primary" href="checkout.html">Checkout</a></div>
											</div>
										  </article>
										</div>-->
									</li>
								</ul>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<section class="section swiper-container swiper-slider swiper-creative bg-gray-2" data-loop="true" data-autoplay="4000" data-simulate-touch="false" data-lazy-loading="true">
			<div class="swiper-wrapper">
				<div class="swiper-slide" data-slide-bg="{{ Vite::image('basketball/slider-slide-1-1920x711.jpg') }}">
					<div class="container">
						<div class="swiper-slide-caption">
						  <!--<p class="text-style-2" data-caption-animate="sliceInLeft"><span>April 15, 2018</span></p>-->
							<h2 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">Som</h2>
							<h1 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">club</h1>
							<article class="quote-minimal" data-caption-animate="sliceInLeft" data-caption-delay="200">
								<svg class="quote-minimal-mark" x="0px" y="0px" width="35.36px" height="24.025px" viewbox="0 0 35.36 24.025">
								<path d="M8.132,0h9.241l-6.53,24.025H0L8.132,0z M26.243,0h9.117l-6.407,24.025H18.111L26.243,0z"></path>
								</svg>
								<p>Som el Club Tennis Taula Ateneu 1882 de Sant Joan Despí. Portem més de 50 anys promovent la pràctica del tennis de taula.</p>
							</article><a class="button button-gray-outline" data-caption-animate="sliceInLeft" data-caption-delay="300" href="about-us.html">Llegir més</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide" data-slide-bg="{{ Vite::image('basketball/slider-slide-2-1920x711.jpg') }}">
					<div class="container">
						<div class="swiper-slide-caption">
						  <!--<p class="text-style-2" data-caption-animate="sliceInLeft"><span>April 15, 2018</span></p>-->
							<h2 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">Som</h2>
							<h1 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">escola</h1>
							<article class="quote-minimal" data-caption-animate="sliceInLeft" data-caption-delay="200">
								<svg class="quote-minimal-mark" x="0px" y="0px" width="35.36px" height="24.025px" viewbox="0 0 35.36 24.025">
								<path d="M8.132,0h9.241l-6.53,24.025H0L8.132,0z M26.243,0h9.117l-6.407,24.025H18.111L26.243,0z"></path>
								</svg>
								<p>Disposem de grups d'entrenament per a totes les edats i nivells. Sigui quina sigui la teva experiència prèvia tenim un lloc per a tu al nostre club.</p>
							</article><a class="button button-gray-outline" data-caption-animate="sliceInLeft" data-caption-delay="300" href="about-us.html">Llegir més</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide" data-slide-bg="{{ Vite::image('basketball/slider-slide-3-1920x711.jpg') }}">
					<div class="container">
						<div class="swiper-slide-caption">
						  <!--<p class="text-style-2" data-caption-animate="sliceInLeft"><span>April 15, 2018</span></p>-->
							<h2 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">Som</h2>
							<h1 class="text-style-3" data-caption-animate="sliceInLeft" data-caption-delay="100">competició</h1>
							<article class="quote-minimal" data-caption-animate="sliceInLeft" data-caption-delay="200">
								<svg class="quote-minimal-mark" x="0px" y="0px" width="35.36px" height="24.025px" viewbox="0 0 35.36 24.025">
								<path d="M8.132,0h9.241l-6.53,24.025H0L8.132,0z M26.243,0h9.117l-6.407,24.025H18.111L26.243,0z"></path>
								</svg>
								<p>Participem individualment i per equips en diverses competicions. Podràs posar-te a prova i aplicar tot el que has après a l'entrenament.</p>
							</article><!--<a class="button button-gray-outline" data-caption-animate="sliceInLeft" data-caption-delay="300" href="shop.html">Read More</a>-->
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-button swiper-button-prev"></div>
			<div class="swiper-button swiper-button-next"></div>
			<div class="swiper-pagination"></div>
		</section>

		<!-- Latest News-->
		<section class="section section-md bg-gray-100">
			<div class="container">
				<div class="row row-50">
					<!--<div class="col-md-12 owl-carousel-outer-navigation">
					  <article class="heading-component">
						<div class="heading-component-inner">
						  <h5 class="heading-component-title">Upcoming events
						  </h5>
						  <div class="owl-carousel-arrows-outline">
							<div class="owl-nav">
							  <button class="owl-arrow owl-arrow-prev"></button>
							  <button class="owl-arrow owl-arrow-next"></button>
							</div>
						  </div>
						</div>
					  </article>
		
					  <div class="owl-carousel" data-items="1" data-autoplay="true" data-autoplay-speed="6500" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">
						<div class="promo-creative unit-spacing-lg">
						  <div class="unit unit-md-horizontal align-items-center align-items-md-stretch">
							<div class="unit-body">
							  <div class="promo-creative-meta">
								<time class="promo-creative-time" datetime="2018-12-31"><span class="heading-1">31</span><span class="heading-5">Dec</span></time>
								<div class="promo-creative-meta-description"><span class="text-primary">08:30 pm</span><span>Saturday</span></div>
							  </div>
							  <div class="promo-creative-figure"><img src="{{ Vite::image('basketball/sport-elements-1-357x321.jpg') }}" alt="" width="357" height="321"/>
							  </div>
							</div>
							<div class="unit-right">
							  <div class="promo-creative-details">
								<h3 class="promo-creative-title">FIBA World Cup</h3>
								<p class="promo-creative-location"><span class="icon icon-gray-800 mdi mdi-map-marker"></span>New Yorkers Stadium
								</p>
								<div class="promo-creative-countdown">
								  <div class="countdown countdown-bordered" data-type="until" data-time="31 Dec 2018 20:30" data-format="dhms" data-style="short"></div>
								</div>
								<div class="promo-main promo-creative-main">
								  <div class="promo-team promo-creative-team">
									<figure class="promo-team-figure"><img src="{{ Vite::image('team-dragons-61x38.png') }}" alt="" width="61" height="38"/>
									</figure>
									<div class="promo-team-title">
									  <div class="promo-team-name">Dragons</div>
									  <div class="promo-team-country">Boston</div>
									</div>
								  </div>
								  <div class="promo-creative-middle">VS</div>
								  <div class="promo-team promo-creative-team">
									<figure class="promo-team-figure"><img src="{{ Vite::image('team-athletic-60x47.png') }}" alt="" width="60" height="47"/>
									</figure>
									<div class="promo-team-title">
									  <div class="promo-team-name">Athletic</div>
									  <div class="promo-team-country">New York</div>
									</div>
								  </div>
								</div>
								<div class="promo-creative-tickets">
								  <div class="promo-buttons text-nowrap">
									<div class="promo-button promo-button-share fl-bigmug-line-share27">
									  <ul class="promo-share-list">
										<li class="promo-share-item"><span>Share</span></li>
										<li class="promo-share-item"><a class="icon fa fa-facebook" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-twitter" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-google-plus" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-instagram" href="#"></a></li>
									  </ul>
									</div><a class="button button-fifth" href="#">Buy tickets</a>
								  </div>
								  <p class="promo-creative-tickets-description">Only<span class="promo-creative-tickets-number">89</span>Tickets Left, Hurry Up</p>
								</div>
							  </div>
							</div>
						  </div>
						</div>
						<div class="promo-creative unit-spacing-lg">
						  <div class="unit unit-md-horizontal align-items-center align-items-md-stretch">
							<div class="unit-body">
							  <div class="promo-creative-meta">
								<time class="promo-creative-time" datetime="2018-05-25"><span class="heading-1">25</span><span class="heading-5">May</span></time>
								<div class="promo-creative-meta-description"><span class="text-primary">04:00 pm</span><span>Tuesday</span></div>
							  </div>
							  <div class="promo-creative-figure"><img src="{{ Vite::image('basketball/sport-elements-2-357x321.jpg') }}" alt="" width="357" height="321"/>
							  </div>
							</div>
							<div class="unit-right">
							  <div class="promo-creative-details">
								<h3 class="promo-creative-title">FIBA World Cup</h3>
								<p class="promo-creative-location"><span class="icon icon-gray-800 mdi mdi-map-marker"></span>New Yorkers Stadium
								</p>
								<div class="promo-creative-countdown">
								  <div class="countdown countdown-bordered" data-type="until" data-time="25 May 2019 16:00" data-format="dhms" data-style="short"></div>
								</div>
								<div class="promo-main promo-creative-main">
								  <div class="promo-team promo-creative-team">
									<figure class="promo-team-figure"><img src="{{ Vite::image('team-athletic-60x47.png') }}" alt="" width="60" height="47"/>
									</figure>
									<div class="promo-team-title">
									  <div class="promo-team-name">Athletic</div>
									  <div class="promo-team-country">New York</div>
									</div>
								  </div>
								  <div class="promo-creative-middle">VS</div>
								  <div class="promo-team promo-creative-team">
									<figure class="promo-team-figure"><img src="{{ Vite::image('team-dragons-61x38.png') }}" alt="" width="61" height="38"/>
									</figure>
									<div class="promo-team-title">
									  <div class="promo-team-name">Dragons</div>
									  <div class="promo-team-country">Boston</div>
									</div>
								  </div>
								</div>
								<div class="promo-creative-tickets">
								  <div class="promo-buttons text-nowrap">
									<div class="promo-button promo-button-share fl-bigmug-line-share27">
									  <ul class="promo-share-list">
										<li class="promo-share-item"><span>Share</span></li>
										li class="promo-share-item"><a class="icon fa fa-facebook" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-twitter" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-google-plus" href="#"></a></li>
										<li class="promo-share-item"><a class="icon fa fa-instagram" href="#"></a></li>
									  </ul>
									</div><a class="button button-fifth" href="#">Buy tickets</a>
								  </div>
								  <p class="promo-creative-tickets-description">Only<span class="promo-creative-tickets-number">15</span>Tickets Left, Hurry Up</p>
								</div>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					</div>-->
					<div class="col-lg-8">
						<div id="news" class="main-component">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Notícies
									</h5><a class="button button-xs button-gray-outline" href="news.html">All News</a>
								</div>
							</article>
							{!! $news !!}
						</div>
						<div class="main-component owl-carousel-outer-navigation">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Latest videos
									</h5>
									<div class="owl-carousel-arrows-outline">
										<div class="owl-nav">
											<button class="owl-arrow owl-arrow-prev"></button>
											<button class="owl-arrow owl-arrow-next"></button>
										</div>
									</div>
								</div>
							</article>

							<!-- Owl Carousel-->
							<div class="owl-carousel" data-items="1" data-autoplay="true" data-autoplay-speed="4000" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe width="560" height="315" src="about:blank" data-src="https://www.youtube.com/embed/42STRZ2DTEM" allowfullscreen=""></iframe>
								</div>
								<div class="embed-responsive embed-responsive-16by9">
									<iframe width="560" height="315" src="about:blank" data-src="https://www.youtube.com/embed/42STRZ2DTEM" allowfullscreen=""></iframe>
								</div>
							</div>
						</div>
						<div class="main-component">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Trending news
									</h5><a class="button button-xs button-gray-outline" href="news.html">All News</a>
								</div>
							</article>

							<div class="row row-30">
								<div class="col-md-6">
									<!-- Post Creative-->
									<article class="post-creative">
										<div class="post-creative-content">
											<div class="post-creative-header">
												<!-- Badge-->
												<div class="badge badge-secondary">The League
												</div>
												<time class="post-creative-time" datetime="2018">April 15, 2018</time>
												<div class="post-creative-view"><span class="icon fl-justicons-visible6"></span>234
												</div>
											</div>
											<h4 class="post-creative-title"><a href="blog-post.html">Forecast: Predictions for each conference semifinal series</a></h4>
										</div><a class="post-creative-figure" href="blog-post.html"><img src="{{ Vite::image('basketball/post-basketball-5-370x279.jpg') }}" alt="" width="370" height="279"/></a>
										<div class="post-creative-footer">
											<div class="post-creative-comment"><span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a></div>
											<div class="post-creative-share">
												<div class="inline-toggle-parent"><span class="post-creative-share-text">Share</span>
													<div class="inline-toggle icon material-icons-share"></div>
													<div class="inline-toggle-element">
														<ul class="list">
															<li><a class="icon fa-facebook" href="#"></a></li>
															<li><a class="icon fa-twitter" href="#"></a></li>
															<li><a class="icon fa-google-plus" href="#"></a></li>
															<li><a class="icon fa-instagram" href="#"></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
								<div class="col-md-6">
									<!-- Post Creative-->
									<article class="post-creative post-creative-compact">
										<div class="post-creative-content">
											<div class="post-creative-header">
												<!-- Badge-->
												<div class="badge badge-primary">Tournament
												</div>
												<time class="post-creative-time" datetime="2018">April 15, 2018</time>
												<div class="post-creative-view"><span class="icon fl-justicons-visible6"></span>234
												</div>
											</div>
											<h4 class="post-creative-title"><a href="blog-post.html">Round 2 predictions: Experts' picks for the conference semifinals</a></h4>
										</div>
										<div class="post-creative-footer">
											<div class="post-creative-comment"><span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a></div>
											<div class="post-creative-share">
												<div class="inline-toggle-parent"><span class="post-creative-share-text">Share</span>
													<div class="inline-toggle icon material-icons-share"></div>
													<div class="inline-toggle-element">
														<ul class="list">
															<li><a class="icon fa-facebook" href="#"></a></li>
															<li><a class="icon fa-twitter" href="#"></a></li>
															<li><a class="icon fa-google-plus" href="#"></a></li>
															<li><a class="icon fa-instagram" href="#"></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</article>
									<!-- Post Creative-->
									<article class="post-creative post-creative-compact">
										<div class="post-creative-content">
											<div class="post-creative-header">
												<!-- Badge-->
												<div class="badge badge-secondary">The League
												</div>
												<time class="post-creative-time" datetime="2018">April 15, 2018</time>
												<div class="post-creative-view"><span class="icon fl-justicons-visible6"></span>234
												</div>
											</div>
											<h4 class="post-creative-title"><a href="blog-post.html">Vote: Pick the winners in the NBA conference semifinals</a></h4>
										</div>
										<div class="post-creative-footer">
											<div class="post-creative-comment"><span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a></div>
											<div class="post-creative-share">
												<div class="inline-toggle-parent"><span class="post-creative-share-text">Share</span>
													<div class="inline-toggle icon material-icons-share"></div>
													<div class="inline-toggle-element">
														<ul class="list">
															<li><a class="icon fa-facebook" href="#"></a></li>
															<li><a class="icon fa-twitter" href="#"></a></li>
															<li><a class="icon fa-google-plus" href="#"></a></li>
															<li><a class="icon fa-instagram" href="#"></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
								<div class="col-md-6">
									<!-- Post Creative-->
									<article class="post-creative">
										<div class="post-creative-content">
											<div class="post-creative-header">
												<!-- Badge-->
												<div class="badge badge-secondary">The Team
												</div>
												<time class="post-creative-time" datetime="2018">April 15, 2018</time>
												<div class="post-creative-view"><span class="icon fl-justicons-visible6"></span>234
												</div>
											</div>
											<h4 class="post-creative-title"><a href="blog-post.html">Six big questions for the Spurs-Rockets heavyweight clash</a></h4>
										</div><a class="post-creative-figure" href="blog-post.html"><img src="{{ Vite::image('basketball/post-basketball-6-370x279.jpg') }}" alt="" width="370" height="279"/></a>
										<div class="post-creative-footer">
											<div class="post-creative-comment"><span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a></div>
											<div class="post-creative-share">
												<div class="inline-toggle-parent"><span class="post-creative-share-text">Share</span>
													<div class="inline-toggle icon material-icons-share"></div>
													<div class="inline-toggle-element">
														<ul class="list">
															<li><a class="icon fa-facebook" href="#"></a></li>
															<li><a class="icon fa-twitter" href="#"></a></li>
															<li><a class="icon fa-google-plus" href="#"></a></li>
															<li><a class="icon fa-instagram" href="#"></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
								<div class="col-md-6">
									<!-- Post Creative-->
									<article class="post-creative">
										<div class="post-creative-content">
											<div class="post-creative-header">
												<!-- Badge-->
												<div class="badge badge-primary">The League
												</div>
												<time class="post-creative-time" datetime="2018">April 15, 2018</time>
												<div class="post-creative-view"><span class="icon fl-justicons-visible6"></span>234
												</div>
											</div>
											<h4 class="post-creative-title"><a href="blog-post.html">Which team will win the world championship series? </a></h4>
										</div><a class="post-creative-figure" href="blog-post.html"><img src="{{ Vite::image('basketball/post-basketball-7-370x279.jpg') }}" alt="" width="370" height="279"/></a>
										<div class="post-creative-footer">
											<div class="post-creative-comment"><span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a></div>
											<div class="post-creative-share">
												<div class="inline-toggle-parent"><span class="post-creative-share-text">Share</span>
													<div class="inline-toggle icon material-icons-share"></div>
													<div class="inline-toggle-element">
														<ul class="list">
															<li><a class="icon fa-facebook" href="#"></a></li>
															<li><a class="icon fa-twitter" href="#"></a></li>
															<li><a class="icon fa-google-plus" href="#"></a></li>
															<li><a class="icon fa-instagram" href="#"></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
						<div class="main-component owl-carousel-outer-navigation">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Shop
									</h5>
									<div class="owl-carousel-arrows-outline">
										<div class="owl-nav">
											<button class="owl-arrow owl-arrow-prev"></button>
											<button class="owl-arrow owl-arrow-next"></button>
										</div>
									</div>
								</div>
							</article>

							<!-- Owl Carousel-->
							<div class="owl-carousel owl-spacing-1" data-items="1" data-autoplay="true" data-autoplay-speed="6500" data-md-items="2" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">
								<article class="product">
									<header class="product-header">
										<!-- Badge-->
										<div class="badge badge-red">hot<span class="icon material-icons-whatshot"></span>
										</div>
										<div class="product-figure"><img src="{{ Vite::image('shop/product-small-3.png') }}" alt=""/></div>
										<div class="product-buttons">
											<div class="product-button product-button-share fl-bigmug-line-share27" style="font-size: 22px">
												<ul class="product-share">
													<li class="product-share-item"><span>Share</span></li>
													<li class="product-share-item"><a class="icon fa fa-facebook" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-instagram" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-twitter" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-google-plus" href="#"></a></li>
												</ul>
											</div><a class="product-button fl-bigmug-line-shopping202" href="shopping-cart.html" style="font-size: 26px"></a><a class="product-button fl-bigmug-line-zoom60" href="{{ Vite::image('shop/product-3-original.jpg') }}" data-lightgallery="item" style="font-size: 25px"></a>
										</div>
									</header>
									<footer class="product-content">
										<h6 class="product-title"><a href="product-page.html">Nike distressed baseball hat</a></h6>
										<div class="product-price"><span class="product-price-old">$400</span><span class="heading-6 product-price-new">$290</span>
										</div>
										<ul class="product-rating">
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star_half"></span></li>
										</ul>
									</footer>
								</article>
								<article class="product">
									<header class="product-header">
										<!-- Badge-->
										<div class="badge badge-green">New
										</div>
										<div class="product-figure"><img src="{{ Vite::image('shop/product-small-2.png') }}" alt=""/></div>
										<div class="product-buttons">
											<div class="product-button product-button-share fl-bigmug-line-share27" style="font-size: 22px">
												<ul class="product-share">
													<li class="product-share-item"><span>Share</span></li>
													<li class="product-share-item"><a class="icon fa fa-facebook" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-instagram" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-twitter" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-google-plus" href="#"></a></li>
												</ul>
											</div><a class="product-button fl-bigmug-line-shopping202" href="shopping-cart.html" style="font-size: 26px"></a><a class="product-button fl-bigmug-line-zoom60" href="{{ Vite::image('shop/product-2-original.jpg') }}" data-lightgallery="item" style="font-size: 25px"></a>
										</div>
									</header>
									<footer class="product-content">
										<h6 class="product-title"><a href="product-page.html">Nike Air Zoom Pegasus</a></h6>
										<div class="product-price"><span class="heading-6 product-price-new">$290</span>
										</div>
										<ul class="product-rating">
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star_half"></span></li>
										</ul>
									</footer>
								</article>
								<article class="product">
									<header class="product-header">
										<!-- Badge-->
										<div class="badge badge-shop">new
										</div>
										<div class="product-figure"><img src="{{ Vite::image('shop/product-small-7.png') }}" alt=""/></div>
										<div class="product-buttons">
											<div class="product-button product-button-share fl-bigmug-line-share27" style="font-size: 22px">
												<ul class="product-share">
													<li class="product-share-item"><span>Share</span></li>
													<li class="product-share-item"><a class="icon fa fa-facebook" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-instagram" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-twitter" href="#"></a></li>
													<li class="product-share-item"><a class="icon fa fa-google-plus" href="#"></a></li>
												</ul>
											</div><a class="product-button fl-bigmug-line-shopping202" href="shopping-cart.html" style="font-size: 26px"></a><a class="product-button fl-bigmug-line-zoom60" href="{{ Vite::image('shop/product-7-original.jpg') }}" data-lightgallery="item" style="font-size: 25px"></a>
										</div>
									</header>
									<footer class="product-content">
										<h6 class="product-title"><a href="product-page.html">garmin vivofit</a></h6>
										<div class="product-price"><span class="heading-6 product-price-new">$290</span>
										</div>
										<ul class="product-rating">
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star"></span></li>
											<li><span class="material-icons-star_half"></span></li>
										</ul>
									</footer>
								</article>
							</div>
						</div>
					</div>
					<!-- Aside Block-->
					<div class="col-lg-4">
						<aside class="aside-components">
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title"><span class="icon material-icons-whatshot text-red"> </span>Hot News
										</h5>
									</div>
								</article>

								<!-- List Post minimal-->
								<div class="list-post-minimal">
									<!-- Post Minimal-->
									<article class="post-minimal">
										<div class="post-classic-main">
											<time class="post-classic-time" datetime="2018">4 hours ago
											</time>
											<p class="post-classic-title"><a href="blog-post.html">5-on-5: What moves do Bucks make to build contender around Giannis?</a></p>
										</div>
									</article>
									<!-- Post Minimal-->
									<article class="post-minimal">
										<div class="post-classic-main">
											<time class="post-classic-time" datetime="2018">15 hours ago
											</time>
											<p class="post-classic-title"><a href="blog-post.html">Raptors snap 11-game losing streak against Bulls with 122-120 win</a></p>
										</div>
									</article>
									<!-- Post Minimal-->
									<article class="post-minimal">
										<div class="post-classic-main">
											<time class="post-classic-time" datetime="2018">1 day ago
											</time>
											<p class="post-classic-title"><a href="blog-post.html">Wizards close out the Hawks, face tougher test in the Celtics</a></p>
										</div>
									</article>
									<!-- Post Minimal-->
									<article class="post-minimal">
										<div class="post-classic-main">
											<time class="post-classic-time" datetime="2018">2 days ago
											</time>
											<p class="post-classic-title"><a href="blog-post.html">NBA Lockdown: Bulls Miss Chances. Concern Over LeBron's Minutes?</a></p>
										</div>
									</article>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Latest games results
										</h5>
									</div>
								</article>

								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-athletic-60x47.png') }}" alt="" width="60" height="47"/>
											</figure>
											<div class="game-result-team-name">Athletic</div>
											<div class="game-result-team-country">New York</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score">105
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewbox="0 0 7 21" enable-background="new 0 0 7 21" xml:space="preserve">
													<g>
													<circle cx="3.5" cy="3.5" r="3"></circle>
													<path d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
													</g>
													<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
													</g>
													</svg>
												</div>
												<div class="game-result-score game-result-team-win">99<span class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
											</div>
											<div class="game-results-status">Home</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-dragons-61x38.png') }}" alt="" width="61" height="38"/>
											</figure>
											<div class="game-result-team-name">Dragons</div>
											<div class="game-result-team-country">Boston</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2018-04-14">April 14, 2018</time>
											</li>
										</ul>
									</div>
								</article>
								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-dragons-61x38.png') }}" alt="" width="61" height="38"/>
											</figure>
											<div class="game-result-team-name">Dragons</div>
											<div class="game-result-team-country">Boston</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score">105
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewbox="0 0 7 21" enable-background="new 0 0 7 21" xml:space="preserve">
													<g>
													<circle cx="3.5" cy="3.5" r="3"></circle>
													<path d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
													</g>
													<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
													</g>
													</svg>
												</div>
												<div class="game-result-score game-result-team-win">123<span class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
											</div>
											<div class="game-results-status">Away</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-rockets-64x50.png') }}" alt="" width="64" height="50"/>
											</figure>
											<div class="game-result-team-name">Rockets</div>
											<div class="game-result-team-country">Houston</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2018-04-14">April 14, 2018</time>
											</li>
										</ul>
									</div>
								</article>
								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-bulls-50x50.png') }}" alt="" width="50" height="50"/>
											</figure>
											<div class="game-result-team-name">Bulls</div>
											<div class="game-result-team-country">Chicago</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score">105
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewbox="0 0 7 21" enable-background="new 0 0 7 21" xml:space="preserve">
													<g>
													<circle cx="3.5" cy="3.5" r="3"></circle>
													<path d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
													</g>
													<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
													</g>
													</svg>
												</div>
												<div class="game-result-score game-result-team-win">29<span class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
											</div>
											<div class="game-results-status">Home</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img src="{{ Vite::image('team-dragons-61x38.png') }}" alt="" width="61" height="38"/>
											</figure>
											<div class="game-result-team-name">Dragons</div>
											<div class="game-result-team-country">Boston</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2018-04-14">April 14, 2018</time>
											</li>
										</ul>
									</div>
								</article>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Social media
										</h5>
									</div>
								</article>

								<!-- Button Icon Alternate-->
								<div class="group-flex group-flex-xs"><a class="button-icon-alternate button-icon-alternate-facebook" href="#"><span class="icon fa-facebook"></span>
										<div class="button-icon-alternate-title">Like us</div></a><a class="button-icon-alternate button-icon-alternate-twitter" href="#"><span class="icon fa-twitter"></span>
										<div class="button-icon-alternate-title">Follow Us</div></a><a class="button-icon-alternate button-icon-alternate-instagram" href="#"><span class="icon fa-instagram"></span>
										<div class="button-icon-alternate-title">Follow Us</div></a><a class="button-icon-alternate button-icon-alternate-google" href="#"><span class="icon fa-google-plus"></span>
										<div class="button-icon-alternate-title">Follow Us</div></a></div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Gallery
										</h5>
									</div>
								</article>

								<article class="gallery" data-lightgallery="group">
									<div class="row row-10 row-narrow">
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-1-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-1-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-2-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-2-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-3-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-3-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-4-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-4-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-5-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-5-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4"><a class="thumbnail-creative" data-lightgallery="item" href="{{ Vite::image('basketball/gallery-basketball-6-original.jpg') }}"><img src="{{ Vite::image('basketball/gallery-basketball-6-116x116.jpg') }}" alt=""/>
												<div class="thumbnail-creative-overlay"></div></a>
										</div>
									</div>
								</article>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Team Stats
										</h5>
									</div>
								</article>

								<div class="table-custom-responsive">
									<table class="table-custom table-custom-bordered table-team-statistic">
										<tr>
											<td>
												<p class="team-statistic-counter">109</p>
												<p class="team-statistic-title">Points Per Game</p>
											</td>
											<td>
												<p class="team-statistic-counter">65</p>
												<p class="team-statistic-title">Rebounds Per Game</p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="team-statistic-counter">23.6</p>
												<p class="team-statistic-title">Assists Per Game</p>
											</td>
											<td>
												<p class="team-statistic-counter">102</p>
												<p class="team-statistic-title">Points Allowed</p>
											</td>
										</tr>
									</table>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Standings
										</h5><a class="button button-xs button-gray-outline" href="#">Full Standings</a>
									</div>
								</article>

								<!-- Table team-->
								<div class="table-custom-responsive">
									<table class="table-custom table-standings table-classic">
										<thead>
											<tr>
												<th colspan="2">Team Position</th>
												<th>W</th>
												<th>L</th>
												<th>PTS</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><span>1</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-dragons-42x26.png') }}" alt="" width="42" height="26"/>
													</div>
													<div class="team-title">
														<div class="team-name">Dragons</div>
														<div class="team-country">Boston</div>
													</div>
												</td>
												<td>153</td>
												<td>30</td>
												<td>186</td>
											</tr>
											<tr>
												<td><span>2</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-bulls-35x35.png') }}" alt="" width="35" height="35"/>
													</div>
													<div class="team-title">
														<div class="team-name">Bulls</div>
														<div class="team-country">Chicago</div>
													</div>
												</td>
												<td>120</td>
												<td>30</td>
												<td>186</td>
											</tr>
											<tr>
												<td><span>3</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-rockets-39x30.png') }}" alt="" width="39" height="30"/>
													</div>
													<div class="team-title">
														<div class="team-name">Rockets</div>
														<div class="team-country">Houston</div>
													</div>
												</td>
												<td>100</td>
												<td>30</td>
												<td>186</td>
											</tr>
											<tr>
												<td><span>4</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-athletic-38x30.png') }}" alt="" width="38" height="30"/>
													</div>
													<div class="team-title">
														<div class="team-name">Athletic</div>
														<div class="team-country">New York</div>
													</div>
												</td>
												<td>98</td>
												<td>30</td>
												<td>186</td>
											</tr>
											<tr>
												<td><span>5</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-hawks-35x35.png') }}" alt="" width="35" height="35"/>
													</div>
													<div class="team-title">
														<div class="team-name">Hawks</div>
														<div class="team-country">Atlanta</div>
													</div>
												</td>
												<td>98</td>
												<td>30</td>
												<td>186</td>
											</tr>
											<tr>
												<td><span>6</span></td>
												<td class="team-inline">
													<div class="team-figure"><img src="{{ Vite::image('team-piranha-35x36.png') }}" alt="" width="35" height="36"/>
													</div>
													<div class="team-title">
														<div class="team-name">Piranha</div>
														<div class="team-country">Seattle</div>
													</div>
												</td>
												<td>98</td>
												<td>30</td>
												<td>186</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="aside-component owl-carousel-outer-navigation">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Players
										</h5>
										<div class="owl-carousel-arrows-outline">
											<div class="owl-nav"><a class="button button-xs button-gray-outline" href="team.html">All Team</a>
												<button class="owl-arrow owl-arrow-prev"></button>
												<button class="owl-arrow owl-arrow-next"></button>
											</div>
										</div>
									</div>
								</article>

								<!-- Owl Carousel-->
								<div class="owl-carousel" data-items="1" data-autoplay="true" data-autoplay-speed="4100" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">
									<!-- 1-->
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure" href="player-page.html"><img src="{{ Vite::image('basketball/roster-player-1-368x286.jpg') }}" alt="" width="368" height="286"/></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>05</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5><a href="player-page.html">Jose Calderon</a></h5>
													<p>Power forward</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">95</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Recv Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">70</span>
													</article>
												</div>
											</div>
										</div>
									</div>
									<!-- 2-->
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure" href="player-page.html"><img src="{{ Vite::image('basketball/roster-player-2-368x286.jpg') }}" alt="" width="368" height="286"/></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>22</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5><a href="player-page.html">Kent Bazemore</a></h5>
													<p>Small forward</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">95</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Recv Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">70</span>
													</article>
												</div>
											</div>
										</div>
									</div>
									<!-- 3-->
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure" href="player-page.html"><img src="{{ Vite::image('basketball/roster-player-3-368x286.jpg') }}" alt="" width="368" height="286"/></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>21</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5><a href="player-page.html">Sam Wlliams</a></h5>
													<p>Shooting guard</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">95</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Recv Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">70</span>
													</article>
												</div>
											</div>
										</div>
									</div>
									<!-- 4-->
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure" href="player-page.html"><img src="{{ Vite::image('basketball/roster-player-4-368x286.jpg') }}" alt="" width="368" height="286"/></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>12</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5><a href="player-page.html">James Peterson</a></h5>
													<p>Team Coach</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">95</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Recv Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div><span class="progress-value">70</span>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Latest Comments
										</h5>
									</div>
								</article>

							</div>
							<!-- List Comments Classic-->
							<div class="list-comments-classic">
								<!-- Comment Classic-->
								<div class="comment-classic">
									<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px" viewbox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68" xml:space="preserve">
									<path fill="#171617" d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
									</svg>
									<div class="comment-classic-header">
										<div class="comment-classic-header-aside"><img src="{{ Vite::image('user-1-63x63.jpg') }}" alt="" width="63" height="63"/>
										</div>
										<div class="comment-classic-header-main">
											<p class="comment-classic-title">Amanda Norton</p>
											<time class="comment-classic-time" datetime="2018">4 hours ago
											</time>
										</div>
									</div>
									<div class="comment-classic-body">
										<div class="comment-classic-text">
											<p>I am not really a big fan of basketball but even I am sure that Rockets are going to make it this time just like they did 3 years...</p>
										</div>
										<div class="comment-classic-post-title"><a href="blog-post.html">Six Big Questions for the Spurs-Rockets Heavyweight Clash</a></div>
									</div>
								</div>
								<!-- Comment Classic-->
								<div class="comment-classic">
									<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px" viewbox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68" xml:space="preserve">
									<path fill="#171617" d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
									</svg>
									<div class="comment-classic-header">
										<div class="comment-classic-header-aside"><img src="{{ Vite::image('user-2-63x63.jpg') }}" alt="" width="63" height="63"/>
										</div>
										<div class="comment-classic-header-main">
											<p class="comment-classic-title">Robert Norton</p>
											<time class="comment-classic-time" datetime="2018">14 hours ago
											</time>
										</div>
									</div>
									<div class="comment-classic-body">
										<div class="comment-classic-text">
											<p>Bulls are my personal favorites, and I don’t...</p>
										</div>
										<div class="comment-classic-post-title"><a href="blog-post.html">Which Team will Win the World Championship Series This Year ?</a></div>
									</div>
								</div>
								<!-- Comment Classic-->
								<div class="comment-classic">
									<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px" viewbox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68" xml:space="preserve">
									<path fill="#171617" d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
									</svg>
									<div class="comment-classic-header">
										<div class="comment-classic-header-aside"><img src="{{ Vite::image('user-3-63x63.jpg') }}" alt="" width="63" height="63"/>
										</div>
										<div class="comment-classic-header-main">
											<p class="comment-classic-title">Rebecca Smith</p>
											<time class="comment-classic-time" datetime="2018">20 hours ago
											</time>
										</div>
									</div>
									<div class="comment-classic-body">
										<div class="comment-classic-text">
											<p>My opinion on the topic is that Spurs don’t have enough chances to withstand the...</p>
										</div>
										<div class="comment-classic-post-title"><a href="blog-post.html">Six Big Questions for the Spurs-Rockets Heavyweight Clash</a></div>
									</div>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Join our Newsletter
										</h5>
									</div>
								</article>

								<!-- Mail Form Modern-->
								<form class="rd-mailform rd-mailform-modern" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
									<div class="form-wrap">
										<label class="form-label" for="subscribe-email">Enter Your E-mail</label>
										<input class="form-input" id="subscribe-email" type="email" name="email">
									</div>
									<div class="form-wrap">
										<button class="button button-block button-primary" type="submit">Subscribe</button>
									</div>
								</form>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</section>

		<!-- Gallery-->
		<section class="section section-xs bg-gray-100">
			<div class="owl-carousel owl-carousel-mystic" data-items="1" data-sm-items="2" data-lg-items="3" data-xl-items="4" data-autoplay="true" data-autoplay-speed="6750" data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-lightgallery="group-owl"><a class="thumbnail-alice" href="{{ Vite::image('basketball/basketball-gallery-1-1200x800-original.jpg') }}" data-lightgallery="item">
					<figure class="thumbnail-alice-figure"><img class="thumbnail-alice-image" src="{{ Vite::image('basketball/basketball-gallery-1-419x298.jpg') }}" alt="" width="419" height="298"/>
					</figure></a><a class="thumbnail-alice" href="{{ Vite::image('basketball/basketball-gallery-2-1200x801-original.jpg') }}" data-lightgallery="item">
					<figure class="thumbnail-alice-figure"><img class="thumbnail-alice-image" src="{{ Vite::image('basketball/basketball-gallery-2-419x298.jpg') }}" alt="" width="419" height="298"/>
					</figure></a><a class="thumbnail-alice" href="{{ Vite::image('basketball/basketball-gallery-3-1200x800-original.jpg') }}" data-lightgallery="item">
					<figure class="thumbnail-alice-figure"><img class="thumbnail-alice-image" src="{{ Vite::image('basketball/basketball-gallery-3-419x298.jpg') }}" alt="" width="419" height="298"/>
					</figure></a><a class="thumbnail-alice" href="{{ Vite::image('basketball/basketball-gallery-4-1200x831-original.jpg') }}" data-lightgallery="item">
					<figure class="thumbnail-alice-figure"><img class="thumbnail-alice-image" src="{{ Vite::image('basketball/basketball-gallery-4-419x298.jpg') }}" alt="" width="419" height="298"/>
					</figure></a></div>
		</section>

		<!-- Page Footer-->
		<footer class="section context-dark footer-creative footer-creative-dark">
			<div class="footer-creative-main">
				<div class="footer-creative-main-top">
					<div class="container text-center">
						<div class="row row-20 justify-content-center align-items-center">
							<div class="col-md-2 text-md-left">
								<h5 class="text-abbey">Our Sponsors</h5>
							</div>
							<div class="col-4 col-sm-2 col-lg-2"><a class="link-image" href="#"><img src="{{ Vite::image('basketball/partners-1-121x33.png') }}" alt="" width="121" height="33"/></a></div>
							<div class="col-4 col-sm-2 col-lg-2"><a class="link-image" href="#"><img src="{{ Vite::image('basketball/partners-2-88x59.png') }}" alt="" width="88" height="59"/></a></div>
							<div class="col-4 col-sm-2 col-lg-2"><a class="link-image" href="#"><img src="{{ Vite::image('basketball/partners-3-97x31.png') }}" alt="" width="97" height="31"/></a></div>
							<div class="col-4 col-sm-2 col-lg-2"><a class="link-image" href="#"><img src="{{ Vite::image('basketball/partners-4-95x52.png') }}" alt="" width="95" height="52"/></a></div>
							<div class="col-4 col-sm-2 col-lg-2"><a class="link-image" href="#"><img src="{{ Vite::image('basketball/partners-5-102x53.png') }}" alt="" width="102" height="53"/></a></div>
						</div>
					</div>
				</div>
				<div class="container">
					<hr>
				</div>
				<div class="footer-creative-main-bottom">
					<div class="container">
						<div class="row row-50 justify-content-center justify-content-lg-between">
							<div class="col-md-4 col-lg-5 col-xxl-4 text-center text-lg-left">
								<article class="unit flex-column flex-lg-row align-items-center justify-content-center justify-content-sm-start footer-creative-info">
									<div class="unit-left"><a class="brand brand-md" href="index.html"><img class="brand-logo brand-logo-dark" src="{{ Vite::image('logo-default-129x81.png') }}" alt="" width="129" height="81"/><img class="brand-logo brand-logo-light" src="{{ Vite::image('logo-inverse-129x81.png') }}" alt="" width="129" height="81"/></a></div>
									<div class="unit-body">
										<p>Dragons  provides extensive information on everything concerning our basketball team for fans all over the world.</p>
									</div>
								</article>
								<div class="group-md group-middle"><a class="button button-sm button-gray-outline" href="contact-us.html">Get in Touch</a>
									<div class="align-items-center">
										<ul class="list-inline list-inline-xs">
											<li><a class="icon icon-corporate fa fa-facebook" href="#"></a></li>
											<li><a class="icon icon-corporate fa fa-twitter" href="#"></a></li>
											<li><a class="icon icon-corporate fa fa-google-plus" href="#"></a></li>
											<li><a class="icon icon-corporate fa fa-instagram" href="#"></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-10 col-md-4 col-lg-4 col-xl-3">
								<h5>Latest news</h5>
								<div class="divider-medium divider-secondary"></div>
								<div class="wrap-posts-classic">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-main">
											<p class="post-classic-title"><a href="blog-post.html">Which team will win the world championship series this year?</a></p>
											<div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
												<time datetime="2018">April 15, 2018</time>
											</div>
										</div>
									</article>
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-main">
											<p class="post-classic-title"><a href="blog-post.html">After seven days off, LeBron James and Cavs rout Raptors </a></p>
											<div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
												<time datetime="2018">April 15, 2018</time>
											</div>
										</div>
									</article>
								</div>
							</div>
							<div class="col-10 col-md-4 col-lg-3">
								<h5>Contact us</h5>
								<div class="divider-medium divider-secondary"></div>
								<ul class="list-md">
									<li class="unit unit-horizontal align-items-center">
										<div class="unit-left">
											<svg class="svg-color-abbey svg-sizing-35" x="0px" y="0px" width="27px" height="27px" viewbox="0 0 27 27" preserveAspectRatio="none">
											<path d="M2,26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-8.185c-0.373-0.132-0.711-0.335-1-0.595V19 H6v-1v-1v-1H5v1v2H3v-9H2v1H1V9V8c0-0.552,0.449-1,1-1h1h1h3h0.184c0.078-0.218,0.173-0.426,0.297-0.617C8.397,5.751,9,4.696,9,3.5 C9,1.567,7.434,0,5.5,0S2,1.567,2,3.5C2,4.48,2.406,5.364,3.056,6H3H2C0.895,6,0,6.895,0,8v7c0,1.104,0.895,2,2,2V26z M8,26H6v-6h2 V26z M5,26H3v-6h2V26z M3,3.5C3,2.121,4.121,1,5.5,1S8,2.121,8,3.5S6.879,6,5.5,6S3,4.879,3,3.5 M1,15v-3h1v4 C1.449,16,1,15.552,1,15"></path>
											<path d="M11.056,6H11h-1C8.895,6,8,6.895,8,8v7c0,1.104,0.895,2,2,2v9c0,0.553,0.447,1,1,1h5 c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2V8c0-1.105-0.896-2-2-2h-1h-0.056C16.594,5.364,17,4.48,17,3.5 C17,1.567,15.434,0,13.5,0S10,1.567,10,3.5C10,4.48,10.406,5.364,11.056,6 M10,15v1c-0.551,0-1-0.448-1-1v-3h1V15z M11,20h2v6h-2 V20z M16,26h-2v-6h2V26z M17,16v-1v-3h1v3C18,15.552,17.551,16,17,16 M17,7c0.551,0,1,0.448,1,1v1v1v1h-1v-1h-1v5v4h-2v-1v-1v-1h-1 v1v1v1h-2v-4v-5h-1v1H9v-1V9V8c0-0.552,0.449-1,1-1h1h1h3h1H17z M13.5,1C14.879,1,16,2.121,16,3.5C16,4.879,14.879,6,13.5,6 S11,4.879,11,3.5C11,2.121,12.121,1,13.5,1"> </path>
											<polygon points="15,13 14,13 14,9 13,9 12,9 12,10 13,10 13,13 12,13 12,14 13,14 14,14 15,14 	"></polygon>
											<polygon points="7,14 7,13 5,13 5,12 6,12 7,12 7,10 7,9 6,9 4,9 4,10 6,10 6,11 5,11 4,11 4,12 4,13 4,14 5,14"></polygon>
											<polygon points="20,10 22,10 22,11 21,11 21,12 22,12 22,13 20,13 20,14 22,14 23,14 23,13 23,12 23,11 23,10 23,9 22,9 20,9 	"></polygon>
											<path d="M19.519,6.383C19.643,6.574,19.738,6.782,19.816,7H20h3h1h1c0.551,0,1,0.448,1,1v3h-1v-1h-1v9 h-2v-2v-1h-1v1v2h-2v-1.78c-0.289,0.26-0.627,0.463-1,0.595V26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2 V8c0-1.105-0.896-2-2-2h-1h-0.056C24.594,5.364,25,4.48,25,3.5C25,1.567,23.434,0,21.5,0S18,1.567,18,3.5 c0,0.736,0.229,1.418,0.617,1.981C18.861,5.834,19.166,6.14,19.519,6.383 M19,20h2v6h-2V20z M24,26h-2v-6h2V26z M26,15 c0,0.552-0.449,1-1,1v-4h1V15z M21.5,1C22.879,1,24,2.121,24,3.5C24,4.879,22.879,6,21.5,6C20.121,6,19,4.879,19,3.5 C19,2.121,20.121,1,21.5,1"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Join Our Team</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</li>
									<li class="unit unit-horizontal align-items-center">
										<div class="unit-left">
											<svg class="svg-color-abbey svg-sizing-35" x="0px" y="0px" width="72px" height="72px" viewbox="0 0 72 72">
											<path d="M36.002,0c-0.41,0-0.701,0.184-0.931,0.332c-0.23,0.149-0.4,0.303-0.4,0.303l-9.251,8.18H11.58 c-1.236,0-1.99,0.702-2.318,1.358c-0.329,0.658-0.326,1.3-0.326,1.3v11.928l-8.962,7.936V66c0,0.015-0.038,1.479,0.694,2.972 C1.402,70.471,3.006,72,5.973,72h30.03h30.022c2.967,0,4.571-1.53,5.306-3.028c0.736-1.499,0.702-2.985,0.702-2.985V31.338 l-8.964-7.936V11.475c0,0,0.004-0.643-0.324-1.3c-0.329-0.658-1.092-1.358-2.328-1.358H46.575l-9.251-8.18 c0,0-0.161-0.154-0.391-0.303C36.703,0.184,36.412,0,36.002,0z M36.002,3.325c0.49,0,0.665,0.184,0.665,0.184l6,5.306h-6.665 h-6.665l6-5.306C35.337,3.51,35.512,3.325,36.002,3.325z M12.081,11.977h23.92H59.92v9.754v2.121v14.816L48.511,48.762 l-10.078-8.911c0,0-0.307-0.279-0.747-0.548s-1.022-0.562-1.684-0.562c-0.662,0-1.245,0.292-1.686,0.562 c-0.439,0.268-0.747,0.548-0.747,0.548l-10.078,8.911L12.082,38.668V23.852v-2.121v-9.754H12.081z M8.934,26.867v9.015 l-5.091-4.507L8.934,26.867z M63.068,26.867l5.091,4.509l-5.091,4.507V26.867z M69.031,34.44v31.559 c0,0.328-0.103,0.52-0.162,0.771L50.685,50.684L69.031,34.44z M2.971,34.448l18.348,16.235L3.133,66.77 c-0.059-0.251-0.162-0.439-0.162-0.769C2.971,66.001,2.971,34.448,2.971,34.448z M36.002,41.956c0.264,0,0.437,0.057,0.546,0.104 c0.108,0.047,0.119,0.059,0.119,0.059l30.147,26.675c-0.3,0.054-0.79,0.207-0.79,0.207H36.002H5.98H5.972 c-0.003,0-0.488-0.154-0.784-0.207l30.149-26.675c0,0,0.002-0.011,0.109-0.059C35.555,42.013,35.738,41.956,36.002,41.956z"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Contact Us</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-creative-aside footer-creative-darken">
				<div class="container">
					<div class="layout-justify">
						<nav class="nav-minimal">
							<ul class="nav-minimal-list">
								<li><a href="team.html">Team</a></li>
								<li><a href="#">Standings</a></li>
								<li><a href="news.html">News</a></li>
								<li><a href="shop.html">Shop</a></li>
								<li><a href="#">Latest results</a></li>
								<li><a href="#">Schedule</a></li>
							</ul>
						</nav>
						<p class="rights"><span>Dragons</span><span>&nbsp;&copy;&nbsp;</span><span class="copyright-year"></span><span>.&nbsp;</span><a class="link-underline" href="privacy-policy.html">Privacy Policy</a></p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<div class="snackbars" id="form-output-global"></div>
	<script src="js/core.min.js"></script>
	<script src="js/script.js"></script>
	@include('popper::assets')
</body>
</html>