@foreach ($news as $pieceofnews)
@php $big_news = $loop->first && $news->onFirstPage(); $is_compact = !$news->onFirstPage()&&$loop->index>=$news->perPage()-2; $post_type = $big_news ? 'corporate' : 'creative' ;@endphp
@if(!$big_news && !isset($row_30_added))
<div class="row row-30">
	@php $row_30_added = true; @endphp
	@endif
	@if(!$big_news && (!$is_compact || $loop->index==$news->perPage()-2))
	<div class="col-md-6">
		@endif
		<article class="post-{{$post_type}}@if($is_compact) post-{{$post_type}}-compact @endif">
			<div class="post-{{$post_type}}-content">
				<div class="post-{{$post_type}}-header">
					<!--<div class="badge badge-primary">The League</div>-->
					<time class="post-{{$post_type}}-time" datetime="{{Carbon\Carbon::parse($pieceofnews->timestamp)->year}}">{{Carbon\Carbon::parse($pieceofnews->timestamp)->isoFormat('MMMM D, YYYY')}}</time>
					<!--<div class="post-{{$post_type}}-view"><span class="icon fl-justicons-visible6"></span>234</div>-->
				</div>
				<h4 class="post-{{$post_type}}-title">
					<a href="blog-post.html">{{$pieceofnews->summary}}</a>
				</h4>
			</div>
			@if(!$is_compact)
			<a class="post-{{$post_type}}-figure" href="blog-post.html">
				<img src="@isset($pieceofnews->pic_url){{Storage::url("pics/$pieceofnews->pic_url")}}@else{{Vite::image("logo-print-hd-transparent.png")}}@endisset" alt="" width="769" height="416"/>
			</a>
			<div class="post-{{$post_type}}-content">
				<div class="post-{{$post_type}}-text">{!!$pieceofnews->text!!}</div>
			</div>
			<div class="post-{{$post_type}}-footer">
				<div class="post-{{$post_type}}-comment">
					<!--<span class="icon mdi mdi-comment-outline"></span><a href="#">345 Comments</a>-->
				</div>
				<div class="post-{{$post_type}}-share">
					<!--<ul class="group">
						<li>{{__('news.share')}}</li>
						<li><a class="icon fa-facebook" href="#"></a></li>
						<li><a class="icon fa-twitter" href="#"></a></li>
						<li><a class="icon fa-google-plus" href="#"></a></li>
						<li><a class="icon fa-instagram" href="#"></a></li>
					  </ul>-->
				</div>
			</div>
			@endif
		</article>
		@if(!$big_news && (!$is_compact || $loop->index==$news->perPage()-1))
	</div>
	@endif
	@if (!$big_news && $loop->last)
</div>
@endif
@endforeach
{{ $news->links() }}