<div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
	@foreach($available_languages as $available_language)
	@if(!$available_language->is($current_language))
	<a href="language/{{ $available_language['iso_639-1'] }}">
		@endif
		<span class="ml-1" @popper({{ __($available_language['iso_639-1']) }})>
			<img class="flag-thumbnail" src="{{ Vite::image($available_language['iso_639-1'].'.png') }}"/>
		</span>
		@if(!$available_language->is($current_language))
	</a>
	@endif
    @endforeach
</div>