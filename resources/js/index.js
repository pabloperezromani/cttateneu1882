$(document).ready(function () {
    $('a[anchor]').each(function () {
        $(this).on('click', function (event) {
            $('html').animate({scrollTop: $(this.getAttribute('anchor')).offset().top - 75}, 'fast');
        });
    });
    $('#start').css({
        'visibility': 'visible',
        'height': 'auto'
    });
    $('#loading-icon').remove();
});