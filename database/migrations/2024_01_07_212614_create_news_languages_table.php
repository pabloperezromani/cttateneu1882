<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

	/**
	 * Run the migrations.
	 */
	public function up(): void {
		Schema::create('news_languages', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->foreignId('news_id')->constrained();
			$table->foreignId('language_id')->constrained();
			$table->string('headline', 100)->nullable();
			$table->text('body')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void {
		Schema::dropIfExists('news_languages');
	}
};
