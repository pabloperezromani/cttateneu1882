<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

	/**
	 * Run the migrations.
	 */
	public function up(): void {
		Schema::create('resources', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('uri');
			$table->foreignId('owner_user_id')->constrained(table: 'users', indexName: 'images_owner_user_id');
			$table->softDeletes();
		});
		Schema::create('images', function (Blueprint $table) {
			$table->id('resource_id');
			$table->foreign('resource_id')->references('id')->on('resources');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void {
		Schema::dropIfExists('images');
		Schema::dropIfExists('resources');
	}
};
